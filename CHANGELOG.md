# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.8.3] - 2022-04-04

### Changed
* templates: fix a character in the perf report

## [1.8.2] - 2022-03-14

### Changed
* templates: blocks: kernel: use the correct metadata value

## [1.8.1] - 2022-03-14

### Changed
* templates: perf-report: use correct metadata value

## [1.8.0] - 2022-02-24

### Added
* add template and scripts to interface with mmtests. [mmtests](https://github.com/gormanm/mmtests) is a 
benchmarking framework primarily aimed at Linux kernel testing.

### Changed
* templates: blocks: kernel: use metadata name from tuxsuite

## [1.7.0] - 2021-12-30

### Added
* all: add the download_url metadata field to all metric regressions

## [1.6.1] - 2021-12-03

### Changed
* reports: replace with an actual key

## [1.6.0] - 2021-11-30

### Added
* all: add metric regressions and fixes when comparing builds
* tests: add a smoke test that has metrics data

### Changed
* squad-find-regressions: return ok if there are no failing tests

## [1.5.0] - 2021-11-21
### Added
* Revert "all: remove arm64 docker builds"

### Changed
* gitlab-ci: use a new set of tags for the arm64 runner
* dockerfile: add the squad-find-regressions script

## [1.4.0] - 2021-11-18
### Added
* all: add basic integration tests
* all: add a short name for the version option
* requirements-dev: add wheel package
* tests/integration: add some basic smoke tests
* squad-find-regressions: determine if a failure is a regression

### Changed
* all: report to stdout by default
* all: reorganize the integration tests
* gitlab-ci: add tls certs directory
* all: update the argument for compare_builds

### Removed
* all: remove arm64 docker builds

## [1.3.0] - 2021-09-14
### Added
* New option, `--send-on`, only generate the report if there are regressions or failures (@aroxell)(!96)

### Changed
* Fix the email subject in the build report (@jscook2345)(!89)

## [1.2.0] - 2021-07-04
### Added
* New option, `--config`, that specifies a path to a config file (@jscook2345)(!83)
* New template, `build`, that details a build without a comparison (@jscook2345)(!85)

## Changed
* Fix error reporting with around base builds (@jscook2345)(!82)
* Render the email subject as a template string (@jscook2345@)(!81)

## [1.1.0] - 2021-06-28
* Unpin the `jinja2` dependency version by using a different template loader (@aroxell)(!74)
* Add an option to cache `squad-client` requests, which requires a newer version of the library (@jscook2345)(!75)

## [1.0.1] - 2021-05-14
### Changed
* Pin the jinja dependency version (@jscook2345)(!70)
* Do not print the cc email header if the config option email_cc is none (@jscook2345)(!68)

## [1.0.0] - 2021-04-16
### Added
* New option, `--version`, prints the version and exits (@aroxell)(!56)
* Print the `--output` argument after a report is created (@aroxell)(!59)
* User configuration file saves both options and template values for later use (@aroxell)(!61,!63)
* New options, `--email-from`, `--email-subject`, `--email-to`, `--email-cc` adds email headers to the report (@aroxell)(!61,!63)
* The `Reported-by` and `Tested-by` template values are now set in the config (@aroxell)(!61,!63)
* The `Signature` template values are now set in the config (@aroxell)(!61,!63)
* New option, `--config-report-type`, specify a report to create from the config (@aroxell)(!61,!63)

### Changed
* The project was renamed `squad-report` (@aroxell)(!62)
* The project library (what you would import) was renamed `squad_report` (@aroxell)(!62)
* The project package (what you would install from `pip`) was renamed `squad-report` (@aroxell)(!62)
* The project project page changed to https://gitlab.com/Linaro/lkft/reports/squad-report (@aroxell)(!62)
* The `squad_report` program was renamed `squad-report` (@aroxell)(!62)
* `--template=full_report` renamed `--template=report-full` (@aroxell)(!63)

## [0.0.2] - 2021-03-15
### Added
* New option, `--unfinished`, that allows comparing unfinished builds (@jscook2345)(!34)
* Examples to the `README.md` (@jscook2345)(!38)
* New document, `CONTRIBUTING.md` (@jscook2345)(!41)
* New option, `--output`, that writes the report to a specified file (@aroxell)(!35)
* New option, `--template`, that specifies which template to use (@aroxell)(!35)
* Custom logging (@aroxell)(!39)
* Reports now show `Reported-by` or `Tested-by` (@aroxell)(!46)

### Changed
* Template improvements based on user feedback (@aroxell)(!35, !36, !48) (@jscook2345)(!33)
* Performance improvements (@aroxell)(!39)

### Removed
* `squad_data` command (@jscook2345)(!37)
* `report` argument, replaced by the `--template` option (@aroxell)(!35)
* `stable` report, replaced by `--template=report` (@aroxell)(!35)
* `full` report, replaced by `--template=full_report` (@aroxell)(!35)
* `short` report (@aroxell)(!51)
* `sanity` report (@aroxell)(!51)

## [0.0.1] - 2021-03-02
### Added
- Initial release of squaddata.
