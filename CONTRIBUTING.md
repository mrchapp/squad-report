# Contributing

This project is managed on `gitlab` at https://gitlab.com/Linaro/lkft/reports/squad-report.

## Open an Issue

Open issues at https://gitlab.com/Linaro/lkft/reports/squad-report/-/issues

## Open a Merge Request

Open merge requests at https://gitlab.com/Linaro/lkft/reports/squad-report/-/merge_requests

For major changes, please open an issue first to discuss what you would like to change.

## Setup a Local Development Environment

1. Fork the `squad-report` project on `gitlab`
2. Clone your fork
```
git clone git@gitlab.com:<username>/squad-report.git
```
3. Create a virtual environment to install development requirements to
```
cd squad_report
python -m venv .venv
source .venv/bin/activate
# to exit your virtual environment
deactivate
```
4. Install the development requirements using [pip](https://pip.pypa.io/en/stable/)
```
pip install --upgrade pip -r requirements-dev.txt
```
5. Create a branch for your change
```
git checkout -b name-of-your-feature
```
6. Test your changes using local development scripts
```
./squad-report --group=lkft --project=linux-next-master --build=next-20210223 --template=report-full --output=report-full.txt
```
7. Make sure your changes pass our format and lint checks
```
black --check --diff --verbose .
flake8 --extend-exclude=.venv .
```
8. Commit your changes and push your branch to `gitlab`.
```
git add .
git commit -m "Your description of your change"
git push origin name-of-your-feature
```
9. Open a merge request on `gitlab`.
