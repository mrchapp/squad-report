from pathlib import Path
import pytest
import shlex
import subprocess
import testfixtures

testdata = Path.cwd().joinpath("tests", "integration", "testdata")


def run(args):
    args = f"squad-report {args}"

    result = subprocess.run(
        shlex.split(args),
        check=True,
        capture_output=True,
        text=True,
    )

    return result


@pytest.mark.parametrize(
    "args, text",
    [
        ("--help", "help.txt"),
        ("-h", "help.txt"),
    ],
)
def test_help(args, text):
    with open(testdata.joinpath(text)) as fp:
        expected = fp.read()

    got = run(args)

    testfixtures.compare(got.stdout, expected)
