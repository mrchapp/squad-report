from pathlib import Path
import os
import pytest
import shlex
import subprocess
import testfixtures

testdata = Path.cwd().joinpath("tests", "integration", "testdata")
perf_hook = os.environ.get("PERF_HOOK", "/")


def run(args):
    args = f"squad-report {args}"

    result = subprocess.run(
        shlex.split(args),
        check=True,
        capture_output=True,
        text=True,
    )

    return result


@pytest.mark.parametrize(
    "args, text",
    [
        (
            "--group=lkft --project=linux-next-master-sanity --build=next-20220401 --base-build=next-20220331 --template=build",  # noqa: E501
            "smoke-linux-next-sanity-build.txt",
        ),
        (
            "--group=lkft --project=linux-next-master-sanity --build=next-20220401 --base-build=next-20220331 --template=report",  # noqa: E501
            "smoke-linux-next-sanity-compare.txt",
        ),
        (
            f"--group=~justin.cook --project=mirror-linux-next-master --build=next-20220401 --base-build=next-20220401 --environments=x86 --suites=mmtests-sysbenchcpu-tests --template=perf-report --perf-report-hook=mmtests --perf-report-hook-args={perf_hook}",  # noqa: E501
            "smoke-mirror-linux-next-sysbenchcpu-x86.txt",
        ),
    ],
)
def test_help(args, text):
    with open(testdata.joinpath(text)) as fp:
        expected = fp.read()

    got = run(args)

    testfixtures.compare(got.stdout, expected)
