## Build
* kernel: 5.17.0
* git: https://gitlab.com/Linaro/lkft/mirrors/next/linux-next
* git branch: master
* git commit: e5071887cd2296a7704dbcd10c1cedf0f11cdbd5
* git describe: next-20220401
* test details: https://qa-reports.linaro.org/lkft/linux-next-master-sanity/build/next-20220401

## Fails
* arm, build
  - arm-clang-13-allmodconfig
  - arm-clang-14-allmodconfig
  - arm-clang-nightly-allmodconfig
  - arm-gcc-10-allmodconfig
  - arm-gcc-11-allmodconfig

* arm64, build
  - arm64-clang-14-allmodconfig
  - arm64-clang-nightly-allmodconfig
  - arm64-gcc-10-allmodconfig
  - arm64-gcc-11-allmodconfig
  - clang-12-lkftconfig
  - clang-13-lkftconfig
  - clang-14-lkftconfig
  - clang-nightly-lkftconfig
  - gcc-11-lkftconfig
  - gcc-11-lkftconfig-64k_page_size
  - gcc-11-lkftconfig-armv8_features
  - gcc-11-lkftconfig-debug
  - gcc-11-lkftconfig-debug-kmemleak
  - gcc-11-lkftconfig-devicetree
  - gcc-11-lkftconfig-kasan
  - gcc-11-lkftconfig-kselftest
  - gcc-11-lkftconfig-kunit
  - gcc-11-lkftconfig-libgpiod
  - gcc-11-lkftconfig-perf
  - gcc-11-lkftconfig-rcutorture

* i386, build
  - i386-clang-13-allmodconfig
  - i386-clang-14-allmodconfig
  - i386-clang-nightly-allmodconfig
  - i386-gcc-10-allmodconfig
  - i386-gcc-11-allmodconfig

* mips, build
  - mips-clang-13-allmodconfig
  - mips-clang-14-allmodconfig
  - mips-clang-nightly-allmodconfig
  - mips-gcc-10-allmodconfig

* parisc, build
  - parisc-gcc-10-allmodconfig
  - parisc-gcc-11-allmodconfig

* powerpc, build
  - powerpc-clang-13-allmodconfig
  - powerpc-clang-13-cell_defconfig
  - powerpc-clang-13-maple_defconfig
  - powerpc-clang-14-allmodconfig
  - powerpc-clang-14-cell_defconfig
  - powerpc-clang-14-maple_defconfig
  - powerpc-clang-nightly-allmodconfig
  - powerpc-clang-nightly-cell_defconfig
  - powerpc-clang-nightly-maple_defconfig

* riscv, build
  - riscv-clang-11-allnoconfig
  - riscv-clang-12-allnoconfig
  - riscv-clang-13-allmodconfig
  - riscv-clang-13-allnoconfig
  - riscv-clang-14-allmodconfig
  - riscv-clang-14-allnoconfig
  - riscv-clang-nightly-allmodconfig
  - riscv-clang-nightly-allnoconfig
  - riscv-gcc-10-allmodconfig
  - riscv-gcc-11-allmodconfig

* s390, build
  - s390-clang-13-allmodconfig
  - s390-clang-14-allmodconfig
  - s390-clang-nightly-allmodconfig

* sh, build
  - sh-gcc-10-allmodconfig
  - sh-gcc-11-allmodconfig

* x15, linux-log-parser
  - check-kernel-warning-4819212

* x86_64, build
  - x86_64-clang-14-allmodconfig
  - x86_64-clang-nightly-allmodconfig


Reported-by: Linux Kernel Functional Testing <lkft@linaro.org>


## Test result summary
total: 5372, pass: 5368, fail: 1, skip: 3, xfail: 0

## Build Summary
* arc: 10 total, 10 passed, 0 failed
* arm: 297 total, 292 passed, 5 failed
* arm64: 49 total, 29 passed, 20 failed
* i386: 44 total, 39 passed, 5 failed
* mips: 41 total, 37 passed, 4 failed
* parisc: 14 total, 12 passed, 2 failed
* powerpc: 65 total, 56 passed, 9 failed
* riscv: 32 total, 22 passed, 10 failed
* s390: 26 total, 23 passed, 3 failed
* sh: 26 total, 24 passed, 2 failed
* sparc: 14 total, 14 passed, 0 failed
* x86_64: 47 total, 45 passed, 2 failed

## Test suites summary
* linux-log-parser
* ltp-smoketest-tests

--
Linaro LKFT
https://lkft.linaro.org
